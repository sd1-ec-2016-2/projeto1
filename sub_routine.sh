#!/bin/bash
DIE=/tmp/die
REST=/tmp/restart_s
SCRIPT=$(pwd)/server/server.js
# /home/mio/gnrl/ufg/sd/2016/projeto1/server/server.js
forever start -a -l server.log -o out.log -e err.log --uid "server_sd1" $SCRIPT > /dev/null

while true; do
if [ -f $DIE ];then
	forever stop $SCRIPT > /dev/null
	rm $DIE
	./stop_proc.sh
fi
if [ -f $REST ];then
	forever stop server_sd1 > /dev/null
	forever start -a -l server.log -o out.log -e err.log --uid "server_sd1" $SCRIPT > /dev/null
	rm $REST
fi
done
