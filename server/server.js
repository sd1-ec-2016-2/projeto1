﻿//load net library
net = require('net');
//file system library
fs = require("fs");
//load our external modules
interpreter = require('../lib/interpreter.js');
clientEntity = require('../lib/clientEntity.js');
clientOfflineEntity = require('../lib/clientOfflineEntity.js');
channelEntity = require ('../lib/channelEntity.js');

//array de sockets/clientes conectados
var clients = [];
//array de objetos com os atributos dos clientes conectados
var clientSpecs = [];
//array de objetos com os atributos dos clientes offline
var clientOfflineSpecs = [];


var channelList = [];
// Array com a lista de canais do servidor

// Se não tiver nenhum canal no servidor, cria um canal com nome Canal1
if(channelList.length == 0){

	var objChannel =channelEntity.constructor();
	objChannel.nick = '#Canal1';
}

// Criando canais para testar chat em canais diferentes

var objChannel2 = channelEntity.constructor();
objChannel2.nick = '#Canal2';
var objChannel3 = channelEntity.constructor();
objChannel3.nick = '#Canal3';

channelList.push(objChannel);
channelList.push(objChannel2);
channelList.push(objChannel3);


net.createServer(function  (socket) {

	//Identifica o cliente que solicitou a conexão
	//socket.name = socket.remoteAddress +":"+ socket.remotePort; (nao esta sendo usado)

	//Objeto cliente para manter registro dos status do cliente
	var objClient = clientEntity.constructor(socket);
	objClient.channels.push(channelList[0]);
	objClient.channel = channelList[0];

	clients.push(socket);
	//Insere cliente na lista/array
	clientSpecs.push(objClient);

	// Endereço do IP em string para usos após a desconexão
	var hostString = socket.remoteAddress;

	// Verifica se o cliente que acabou de conectar está na lista de clientes offline
	for (var i=0; i < clientOfflineSpecs.length; i++)
		{
			if (clientOfflineSpecs[i].socket == socket.remoteAddress)
			{ // se sim, remove o cliente da lista de offline, porque agora ele está online
				clientOfflineSpecs.splice(clientOfflineSpecs.indexOf(i), 1);
			}
		}

	
	//Handler para as mensagens/comandos enviados
	socket.on('data', function(data)
	{

		//data é do tipo Buffer de acordo com a documentação, portanto, convertemos para string
		var tmp = data.toString().trim();
		
		//Caso seja um comando
		if(tmp[0] == '/') interpreter.resolve(clientSpecs, clients, objClient, tmp, channelList, clientOfflineSpecs);
		else if(tmp[0] == '#') interpreter.channelResolve(clientSpecs, clients, objClient, tmp, channelList);
		else if(tmp !== "") broadcast(objClient.nick + ">" + data, objClient);
	});



	//Handler para solicitação de encerramento de conexão
	socket.on('end', function(){


		// adiciona o cliente para a lista de clientes offline
		if (objClient.nick !== null)
		{
			broadcast(objClient.nick + " has left the channel.\n"); //avisa no chat que o usuario saiu
			var objOfflineClient = clientOfflineEntity.constructor();
			objOfflineClient.nick = objClient.nick;
			objOfflineClient.user = objClient.user;
			objOfflineClient.realname = objClient.realname;
			objOfflineClient.socket = hostString;

			// adiciona o cliente a lista de clientes offline
			clientOfflineSpecs.push(objOfflineClient);
		}


		// remove o client do array
		clients.splice(clients.indexOf(socket), 1);
		if (objClient.nick !== null) 
		{
			if(objClient.quitMessage)
				broadcast(objClient.nick + " has left the channel. Reason: "+objClient.quitMessage, objClient); //avisa no chat que o usuario saiu
			else
				broadcast(objClient.nick + " has left the channel.", objClient); //avisa no chat que o usuario saiu	
		}
		// Remove o objeto objClient do vetor clientSpecs
		clientSpecs.splice(clientSpecs.indexOf(objClient), 1);	
	});
}).listen(7194); //porta padrão do IRC


//Implementação da broadcast (envia msg para todos os clientes)
function broadcast(message, sender)
{


	var channel;
	channel = sender.channel;
	// a variável channel pega o servidor que o cliente está mandando a mensagem
	clientSpecs.forEach(function (client)
	{
		if(client === sender) return; //Nao e preciso enviar a msg para o sender/remetente

		for(i=0;i<client.channels.length;i++)
		{
			//Verifica o canal para enviar a mensagem
			if(client.channels[i].nick == channel.nick)
				client.socket.write(channel.nick +" | "+"> "+message+"\n");
		}
	});
}

function broadcastAll(message)
{
	clients.forEach(function (client){
		client.write(message);
	});
	//Escreve na tela do sender também
}

exports.restart = function()
{
	broadcastAll("[SYSTEM]: Restarting server.\n");

	// Asynchronous - Opening File <creating control file>./
	fs.open('/tmp/restart_s', 'w+', function(err, fd) {
	   if (err) {
	      return console.error(err);
	   }
	});
}

exports.die = function()
{
	broadcastAll("[SYSTEM]: Shutting down server.\n");

	// Asynchronous - Opening File <creating control file>
	fs.open('/tmp/die', 'w+', function(err, fd) {
	   if (err) {
	      return console.error(err);
	   }
	});
}