exports.constructor = function(){
	var objChannel = new Object();
	objChannel.nick = null;
	objChannel.clients = [];
	objChannel.modes = ['a', 'i', 'w', 'r', 'o', 'O', 's'];
	objChannel.all_modes = ['a', 'i', 'w', 'r', 'o', 'O', 's'];
	return objChannel;
};
