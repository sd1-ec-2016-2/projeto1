# Projeto 1 da disciplina de Sistemas Distribuídos 1 do curso de Eng. Computação da UFG

## Documentação
A documentação completa do projeto está na [wiki](https://gitlab.com/sd1-ec-2016-2/projeto1/wikis/home).

USAGE
===================
> É necessária instalação do módulo "forever" do nodejs para execução correta do daemon:
> - **sudo npm install forever -g**

> Em seguida, em um terminal, basta executar o daemon.sh
> - **./daemon.sh**

> Para verificar os processos em execução:
> - **ps aux | grep sub_routine.sh**
> - **ps aux | grep server.js**

> Agora, o processo do server está em background não necessitando de uma sessão ativa.

> Faça o teste. Feche o terminal. Abra outro e rode um cliente:
> - **cd client/**
> - **node client.js nick**

> Agora use o comando
> - **>/die**

> Se tudo ocorreu certo, o servidor foi desligado [Shuting down server.] e os processos não existem mais. Confirme:
> - **ps aux | grep sub_routine.sh**
> - **ps aux | grep server.js**


> Ao adicionar novas funcionalidades ao código do projeto como um todo, é mais fácil que o server.js seja executado diretamente (dependete de uma shell), por que os erros encontrados serão printados na tela. Contudo, através da execução do daemon.sh, os erros são salvos em um arquivo chamado err.log na pasta raiz do projeto, tanto quanto o stdout do server é direcionado ao arquivo out.log. Mas, ainda assim, vale ressaltar que em caso de erro no código durante a execução do daemon o processo do server.js provavelmente deverá ser finalizado manualmente, portanto, fikdik:
> - **ps aux | grep server.js**
> - **kill Numero_do_processo**

> Ou, de uma maneira mais fácil usando o módulo do node:
> - **forever stop server_sd1**

Obs.: "server_sd1" é o uid que damos ao processo através de uma opção do módulo forever, confira o comando em "sub_routine.sh".

> Não se esqueça de encerrar o sub_routine.sh caso necessário:
> - **./stop_proc.sh**



TO DO
===================

A implementação da base completa do IRC está em progresso.

Corrigir pequenos bugs
> Pequenos bugs na interpretação dos comandos:
> - Momentos em que "espaços" e "tabs" deveriam ser ignorados mas definem o resultado de um comando;
> - Encontrar e corrigir momentos em que o sinal do console ">" não aparece corretamente precedendo uma mensagem;
> - Encontrar e remover variáveis em desuso.


Arquivos
-------------
> - **/server/server.js**
> - **/lib/interpreter.js**
> - **/lib/clientEntity.js**
> - **/lib/channelEntity.js**
> - **/client/client.js**
> - **daemon.sh**
> - **sub_routine.sh**
> - **stop_proc.sh**
> - **README.md**


**Ler todo RFC.**
